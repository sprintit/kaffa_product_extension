# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2019 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################

import logging
logger = logging.getLogger(__name__)

from openerp import models, fields, api, _

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    brand = fields.Char('Brand', help='Vendor brand')
