# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2019 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################


{
    'name': 'Product Extension',
    'version': '0.1',
    'license': 'Other proprietary',
    'category': 'sale',
    'description': 'Module is to add some custom fields on the product card and for future customizations too.',
    'summary':'Some custom fields to Product Card',
    'author': 'SprintIT Ltd., Harri Matero',
    'maintainer': 'SprintIT Ltd.',
    'website': 'http://www.sprintit.fi',
    'depends': [
        'sale_management',
    ],
    'data': [
        'view/product_template_view.xml'
    ],
    'installable': True,
    'auto_install': False,
 }
